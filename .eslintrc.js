function onProduction(level, notProdLevel = "off") {
  return process.env.NODE_ENV === "production" ? level : notProdLevel;
}

module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    "plugin:vue/recommended",
    "plugin:prettier/recommended",
    "@vue/prettier"
  ],
  rules: {
    "no-console": onProduction("error"),
    "no-debugger": onProduction("error"),
    "vue/html-indent": ["error", 4]
  },
  parserOptions: {
    parser: "babel-eslint"
  }
};
